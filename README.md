# Gym.Basket

Gym.Basket requires dotnet core version 2.2

The class library is Gym.Basket.

The unit tests are in Gym.Basket.Tests.

To run the tests

`dotnet test .\Gym.Basket.Tests\`

