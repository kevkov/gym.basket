using System;

namespace Gym.Basket
{
    public class ProductItem
    {
        public Product Product { get; }
        public int Quantity { get; }

        public ProductItem(Product product, int quantity)
        {
            if (product == null) throw new ArgumentNullException(nameof(product));
            if (quantity <= 0) throw new ArgumentOutOfRangeException(nameof(quantity));
            Product = product;
            Quantity = quantity;
        }

        public decimal TotalCost => Product.Price * Quantity;

        public ProductItem AddMore(int quantity)
        {
            if (quantity <= 0) throw new ArgumentOutOfRangeException(nameof(quantity)); 
            return new ProductItem(Product, Quantity + quantity);
        }
    }
}