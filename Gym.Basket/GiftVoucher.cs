using System;

namespace Gym.Basket
{
    public class GiftVoucher
    {
        public Guid Id { get; }
        public string Name { get; }
        public decimal Amount { get; }

        public GiftVoucher(Guid id, string name, decimal amount)
        {
            if (id == Guid.Empty)
                throw new ArgumentException("Value cannot be empty.", nameof(id));
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(name));
            if (amount <= 0) throw new ArgumentOutOfRangeException(nameof(amount));
            
            Id = id;
            Name = name;
            Amount = amount;
        }
    }
}