using System;
using System.Collections.Generic;
using System.Linq;

namespace Gym.Basket
{
    public abstract class OfferVoucher
    {
        public Guid Id { get; }
        public string Name { get; }
        public decimal Threshold { get; }
        public decimal Amount { get; }

        public OfferVoucher(Guid id, string name, decimal threshold, decimal amount)
        {
            if (id == Guid.Empty)
                throw new ArgumentException("Value cannot be empty.", nameof(id));
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(name));
            // or < some practical minimum value
            if (threshold <= 0) throw new ArgumentOutOfRangeException(nameof(threshold));
            if (amount <= 0) throw new ArgumentOutOfRangeException(nameof(amount));
            Id = id;
            Name = name;
            Threshold = threshold;
            Amount = amount;
        }

        public abstract List<Product> GetApplicableProducts(List<Product> products);
    }

    public class AnyProductOfferVoucher : OfferVoucher
    {
        public AnyProductOfferVoucher(Guid id, string name, decimal threshold, decimal amount) 
            : base(id, name, threshold, amount)
        {
        }

        public override List<Product> GetApplicableProducts(List<Product> products)
        {
            return products;
        }
    }

    public class ProductCategoryOfferVoucher : OfferVoucher
    {
        public ProductCategory ProductCategory { get; }

        public ProductCategoryOfferVoucher(Guid id, string name, decimal threshold, decimal amount, ProductCategory productCategory) 
            : base(id, name, threshold, amount)
        {
            ProductCategory = productCategory;
        }

        public override List<Product> GetApplicableProducts(List<Product> products)
        {
            return products.Where(p => p.ProductCategory.Id == ProductCategory.Id).ToList();
        }
    }
}
