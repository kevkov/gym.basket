using System;
using FluentAssertions;
using NUnit.Framework;

namespace Gym.Basket.Tests
{
    public class BasketTests
    {
        [Test]
        public void WhenMultipleProductsAdded_ThenTotalIsCorrect()
        {
            // Arrange
            var sut = new Basket();

            // Act
            sut.Add(new Product(Guid.NewGuid(), "Hat", 10.5m, new ProductCategory(Guid.NewGuid(), "Hats")));
            sut.Add(new Product(Guid.NewGuid(), "Jumper", 54.65m, new ProductCategory(Guid.NewGuid(), "Jumpers")));

            // Assert
            sut.TotalPrice.Should().Be(65.15m);
        }

        [Test]
        public void WhenSameProductAddedTwice_ThenTotalAndItemsAreCorrect()
        {
            // Arrange
            var sut = new Basket();
            var product = new Product(Guid.NewGuid(), "Hat", 10.5m, new ProductCategory(Guid.NewGuid(), "Hats"));

            // Act
            sut.Add(product);
            sut.Add(product);

            // Assert
            sut.TotalPrice.Should().Be(21m);
            sut.StandardProducts.Count.Should().Be(1);
            sut.StandardProducts[0].Should().BeEquivalentTo(new ProductItem(product, 2));
        }

        [Test]
        public void Basket1()
        {
            // Arrange
            var sut = new Basket();
            sut.Add(new Product(Guid.NewGuid(), "Hat", 10.5m, new ProductCategory(Guid.NewGuid(), "Hats")));
            sut.Add(new Product(Guid.NewGuid(), "Jumper", 54.65m, new ProductCategory(Guid.NewGuid(), "Jumpers")));

            // Act
            sut.ApplyVoucher(new GiftVoucher(Guid.NewGuid(), "Gift Voucher XXX-XXX ", 5m));

            // Assert
            sut.TotalPrice.Should().Be(60.15m);
        }

        [Test]
        public void Basket2()
        {
            // Arrange
            var sut = new Basket();
            sut.Add(new Product(Guid.NewGuid(), "Hat", 25m, new ProductCategory(Guid.NewGuid(), "Hats")));
            sut.Add(new Product(Guid.NewGuid(), "Jumper", 26m, new ProductCategory(Guid.NewGuid(), "Jumpers")));

            // Act
            var result = sut.ApplyVoucher(new ProductCategoryOfferVoucher(Guid.NewGuid(), "Voucher YYY-YYY", 50m, 5m,
                new ProductCategory(Guid.NewGuid(), "Head Gear")));

            // Assert
            result.Successful.Should().BeFalse();
            result.Message.Should().Be("There are no products in your basket applicable to voucher Voucher YYY-YYY");
            sut.TotalPrice.Should().Be(51m);
        }
        
        [Test]
        public void Basket3()
        {
            // Arrange
            var sut = new Basket();
            sut.Add(new Product(Guid.NewGuid(), "Hat", 25m, new ProductCategory(Guid.NewGuid(), "Hats")));
            sut.Add(new Product(Guid.NewGuid(), "Jumper", 26m, new ProductCategory(Guid.NewGuid(), "Jumpers")));
            var headerGearCatgeoryId = Guid.NewGuid();
            sut.Add(new Product(Guid.NewGuid(), "Head Light", 3.5m, new ProductCategory(headerGearCatgeoryId, "Head Gear")));

            // Act
            var result = sut.ApplyVoucher(new ProductCategoryOfferVoucher(Guid.NewGuid(), "Voucher YYY-YYY", 50m, 5m,
                new ProductCategory(headerGearCatgeoryId, "Head Gear")));

            // Assert
            result.Successful.Should().BeTrue();
            sut.TotalPrice.Should().Be(51m);
        }
        
        [Test]
        public void Basket4()
        {
            // Arrange
            var sut = new Basket();
            sut.Add(new Product(Guid.NewGuid(), "Hat", 25m, new ProductCategory(Guid.NewGuid(), "Hats")));
            sut.Add(new Product(Guid.NewGuid(), "Jumper", 26m, new ProductCategory(Guid.NewGuid(), "Jumpers")));

            // Act
            sut.ApplyVoucher(new GiftVoucher(Guid.NewGuid(), "Gift Voucher XXX-XXX ", 5m));
            var result = sut.ApplyVoucher(new AnyProductOfferVoucher(Guid.NewGuid(), "Voucher YYY-YYY", 50m, 5m));

            // Assert
            result.Successful.Should().BeTrue();
            sut.TotalPrice.Should().Be(41m);
        }
        
        [Test]
        public void Basket5()
        {
            // Arrange
            var sut = new Basket();
            sut.Add(new Product(Guid.NewGuid(), "Hat", 25m, new ProductCategory(Guid.NewGuid(), "Hats")));
            sut.Add(new GiftVoucherProduct(Guid.NewGuid(), "GiftVoucher", 30m));

            // Act
            var result = sut.ApplyVoucher(new AnyProductOfferVoucher(Guid.NewGuid(), "Voucher YYY-YYY", 50m, 5m));

            // Assert
            result.Successful.Should().BeFalse();
            result.Message.Should()
                .Be("You have not reached the spend threshold for Voucher YYY-YYY. Spend another £25.01 to receive £5.00 discount from your basket total");
            sut.TotalPrice.Should().Be(55m);
        }
        
        [Test]
        public void Basket6()
        {
            // Arrange
            var sut = new Basket();
            var product = new Product(Guid.NewGuid(), "Hat", 10.5m, new ProductCategory(Guid.NewGuid(), "Hats"));
            sut.Add(product);
            sut.Add(product);
            
            // Act
            var result = sut.ApplyVoucher(new AnyProductOfferVoucher(Guid.NewGuid(), "Voucher 6", 20m, 5m));

            // Assert
            result.Successful.Should().BeTrue();
            sut.TotalPrice.Should().Be(16m);
        }
    }
}