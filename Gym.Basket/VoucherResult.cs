

namespace Gym.Basket
{
    public class VoucherResult
    {
        public bool Successful { get; }
        public string Message { get; }
        public static readonly VoucherResult Success = new VoucherResult(true);

        public static VoucherResult Failed(string msg)
        {
            return new VoucherResult(false, msg);
        }

        private VoucherResult(bool successful, string message = "")
        {
            Successful = successful;
            Message = message;
        }
    }
}