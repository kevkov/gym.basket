using System;

namespace Gym.Basket
{
    public class Product
    {
        public Guid Id { get; }
        public string Name { get; }
        public decimal Price { get; }
        public ProductCategory ProductCategory { get; }

        public Product(Guid id, string name, decimal price, ProductCategory productCategory)
        {
            if (id == Guid.Empty)
                throw new ArgumentException("Value cannot be empty.", nameof(id));
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(name));
            if (price < 0) throw new ArgumentOutOfRangeException(nameof(price));
            if (productCategory == null) throw new ArgumentNullException(nameof(productCategory));
            
            Id = id;
            Name = name;
            Price = price;
            ProductCategory = productCategory;
        }
    }
}