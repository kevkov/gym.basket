﻿using System;
using System.Collections.Generic;
using System.Linq;
using static Gym.Basket.VoucherResult;

namespace Gym.Basket
{
    public class Basket
    {
        public decimal TotalPrice { get; private set; }
        public IReadOnlyList<ProductItem> StandardProducts => _standardProductItems;
        
        private List<ProductItem> _standardProductItems = new List<ProductItem>();
        private List<GiftVoucherProduct> _giftVoucherProducts = new List<GiftVoucherProduct>();
        private List<GiftVoucher> _appliedGiftVouchers = new List<GiftVoucher>();
        private List<OfferVoucher> _appliedOfferVouchers = new List<OfferVoucher>();


        public void Add(Product product, int quantity = 1)
        {
            if (quantity <= 0) throw new ArgumentOutOfRangeException(nameof(quantity));
            var index = _standardProductItems.FindIndex(pi => pi.Product.Id == product.Id);
            if (index == -1)
            {
                _standardProductItems.Add(new ProductItem(product, quantity));           
            }
            else
            {
                _standardProductItems[index] = _standardProductItems[index].AddMore(quantity);
            }
            RecalculateTotalPrice();
        }

        private decimal TotalCostOfStandardProducts => _standardProductItems.Sum(pi => pi.TotalCost);

        private void RecalculateTotalPrice()
        {
            decimal totalCostOfGiftVoucherProducts = _giftVoucherProducts.Sum(p => p.Amount);
            decimal appliedGiftVouchersAmount = _appliedGiftVouchers.Sum(ap => ap.Amount);
            
            var appliedOfferVouchersAmount = CalculateAppliedOfferVouchersAmount();

            TotalPrice = Math.Max(0m, TotalCostOfStandardProducts 
                                      + totalCostOfGiftVoucherProducts 
                                      - appliedGiftVouchersAmount 
                                      - appliedOfferVouchersAmount);
        }

        private decimal CalculateAppliedOfferVouchersAmount()
        {
            var allProducts = _standardProductItems.Select(p => p.Product).ToList();
            IEnumerable<(OfferVoucher voucher, List<ProductItem> applicableProducts)> vps =
                _appliedOfferVouchers
                    .Select(av => (av,
                        _standardProductItems.Where(pi =>
                            av.GetApplicableProducts(allProducts).Exists(p => p.Id == pi.Product.Id)).ToList()));

            var appliedOfferVouchersAmount =
                vps.Sum(vp => Math.Min(vp.voucher.Amount, vp.applicableProducts.Sum(pi => pi.TotalCost)));
            return appliedOfferVouchersAmount;
        }

        public void ApplyVoucher(GiftVoucher giftVoucher)
        {
            // to do what if ita already applied?
            _appliedGiftVouchers.Add(giftVoucher);
            RecalculateTotalPrice();
        }

        public VoucherResult ApplyVoucher(OfferVoucher offerVoucher)
        {
            var applicableProducts = offerVoucher.GetApplicableProducts(_standardProductItems.Select(pi => pi.Product).ToList());
            if (applicableProducts.Any() == false)
            {
                // in reality the basket would not be producing messages
                return Failed($"There are no products in your basket applicable to voucher {offerVoucher.Name}");
            }

            // alternatively it might be better to delegate voucher apllicability and
            // threshold criteria completely to the voucher
            if (TotalCostOfStandardProducts > offerVoucher.Threshold)
            {
                _appliedOfferVouchers.Add(offerVoucher);
                RecalculateTotalPrice();
                return Success;
            }

            decimal spendRequired = offerVoucher.Threshold - TotalCostOfStandardProducts + .01m;
            return Failed($"You have not reached the spend threshold for {offerVoucher.Name}. Spend another {spendRequired:C} to receive {offerVoucher.Amount:C} discount from your basket total");
        }

        public void Add(GiftVoucherProduct product)
        {
            _giftVoucherProducts.Add(product);
            RecalculateTotalPrice();
        }
    }
}