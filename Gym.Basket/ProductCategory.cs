using System;

namespace Gym.Basket
{
    public class ProductCategory
    {
        public Guid Id { get; }

        public ProductCategory(Guid id, string name)
        {
            if (id == Guid.Empty)
                throw new ArgumentException("Value cannot be empty.", nameof(id));
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(name));
            
            Id = id;
            Name = name;
        }
        
        public string Name { get; }
    }
}